
let wrapper = document.querySelector("#wrapper");
let colori = ['red', 'blue', 'purple', 'yellow', 'green', 'brown','black'];

document.addEventListener("click", (e) =>{
    let rettangolo = document.createElement("div")
    let indice = Math.floor(Math.random() * 6);

    let altezza = Math.floor(Math.random() * (400 - 50 + 1) ) + 50;
    let larghezza = Math.floor(Math.random() * (400 - 50 + 1) ) + 50;

    rettangolo.style.position =  "absolute";
    rettangolo.style.width =  larghezza + "px";
    rettangolo.style.height = altezza + "px";
    rettangolo.style.left = e.x + "px";
    rettangolo.style.top = e.y + "px";
    rettangolo.style.background = colori[indice];

  
    wrapper.appendChild(rettangolo)
});


  
